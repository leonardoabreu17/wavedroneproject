// Arguments passed into this controller can be accessed via the `$.args` object directly or:

function accelerometerB()
{
	var acceletometerWin = Alloy.createController("accelerometer").getView();
    if(OS_ANDROID)
    {
    		acceletometerWin.open();
    }
    if(OS_IOS) 
    {
    		$.Menu.openWindow(acceletometerWin);
    }
}

function gyroB()
{
	var gyroWin = Alloy.createController("gyro").getView();
    if(OS_ANDROID)
    {
    		gyroWin.open();
    }
    if(OS_IOS) 
    {
    		$.Menu.openWindow(gyroWin);
    }
}

function compassB()
{
	var compassWin = Alloy.createController("compass").getView();
    if(OS_ANDROID)
    {
    		compassWin.open();
    }
    if(OS_IOS) 
    {
    		$.Menu.openWindow(compassWin);
    }
}


function connect()
{
	var connectWin = Alloy.createController("connection").getView();
    if(OS_ANDROID)
    {
    		connectWin.open();
    }
    if(OS_IOS) 
    {
    		$.Menu.openWindow(connectWin);
    }
};