var CoreMotion = require('ti.coremotion');
var Gyroscope = CoreMotion.createGyroscope();
 
if (Gyroscope.isGyroAvailable()) {
    Gyroscope.setGyroUpdateInterval(1000);
    Gyroscope.startGyroUpdates(updateGyroData);
} else {
    alert("Device does not have an gyroscope.");
}
  
 
var progress_bar_args = {
    max: 1,
    min: 0,
    value: 0,
    width: 200,
    top: 50
};
 
var gyroX = Ti.UI.createProgressBar(progress_bar_args);
var gyroY = Ti.UI.createProgressBar(progress_bar_args);
var gyroZ = Ti.UI.createProgressBar(progress_bar_args);
 
$.winGyro.add(gyroX);
$.winGyro.add(gyroY);
$.winGyro.add(gyroZ);
 
gyroX.show();
gyroY.show();
gyroZ.show();

 
function updateGyroData (e) {
    if (e.success) {
        dataG = e.rotationRate;
        
        // Show values just with 3 digits 
	    dataGXout=dataG.x.toFixed(3);
	    dataGYout=dataG.y.toFixed(3);
	    dataGZout=dataG.z.toFixed(3);
    
        
        gyroX.message = 'Gyro X: ' + dataGXout;
        gyroX.value = Math.abs(dataG.x);
        gyroY.message = 'Gyro Y: ' + dataGYout;
        gyroY.value = Math.abs(dataG.y);
        gyroZ.message = 'Gyro Z: ' + dataGZout;
        gyroZ.value = Math.abs(dataG.z);
    } else {
        if (e.error) {
            Ti.API.error(e.error);
        }
    }
} 
