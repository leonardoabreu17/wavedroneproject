var CoreMotion = require('ti.coremotion');


var Accelerometer = CoreMotion.createAccelerometer();
 
if (Accelerometer.isAccelerometerAvailable()) {
    Accelerometer.setAccelerometerUpdateInterval(1000);
    Accelerometer.startAccelerometerUpdates(updateAccelData);
} else {
    alert('Device does not have an accelerometer.');
}
 
// GUI to display measurements
 
var progress_bar_args = {
    max: 1,
    min: 0,
    value: 0,
    width: 200,
    top: 50
};
 
 
var accelX = Ti.UI.createProgressBar(progress_bar_args);
var accelY = Ti.UI.createProgressBar(progress_bar_args);
var accelZ = Ti.UI.createProgressBar(progress_bar_args);
 
$.winAcce.add(accelX);
$.winAcce.add(accelY);
$.winAcce.add(accelZ);
 
 
accelX.show();
accelY.show();
accelZ.show();
 
function updateAccelData (e) {
    data = e.acceleration;
    
    // Show values just with 3 digits 
    dataXout=data.x.toFixed(3);
    dataYout=data.y.toFixed(3);
    dataZout=data.z.toFixed(3);
    
    
    
    accelX.message = "X: " + dataXout;
    accelX.value = Math.abs(data.x);
    accelY.message = "Y: " + dataYout;
    accelY.value = Math.abs(data.y);
    accelZ.message = "Z: " + dataZout;
    accelZ.value = Math.abs(data.z);
}
 

