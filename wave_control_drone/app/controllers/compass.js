
function updateLabels(_args) {
  $.compassHeading.text = _args.heading.magneticHeading+ ' degrees';
  
  var headingText = null;
  var theBearing = _args.heading.magneticHeading;
  switch(true) {
    case (theBearing >= 0 && theBearing < 30):
      headingText = 'North';
      break;
    case (theBearing >= 30 && theBearing < 60):
      headingText = 'North East';
      break;
    case (theBearing >= 60 && theBearing < 120):
      headingText = 'East';
      break;
   case (theBearing >= 120 && theBearing < 150):
      headingText = 'South East';
      break;
   case (theBearing >= 150 && theBearing < 210):
      headingText = 'South';
      break;
   case (theBearing >= 210 && theBearing < 240):
      headingText = 'South West';
      break;
   case (theBearing >= 240 && theBearing < 300):
      headingText = 'West';
      break;
   case (theBearing >= 300 && theBearing < 330):
      headingText = 'North West';
      break;        
// I've cut some of the options out to keep this listing short. This set of cases continues upward until
    case (theBearing >= 330 && theBearing <= 360):
      headingText = 'North';
      break;
             }
  $.direction.text = 'You are looking '+headingText;
}

Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE, function(e) {
Ti.Geolocation.addEventListener("heading", updateLabels);  });
//Ti.Geolocation.addEventListener("heading", updateLabels);
